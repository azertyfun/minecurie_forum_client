import Vue from 'vue'
import VueResource from 'vue-resource'
import Marked from 'marked'
import App from './App.vue'

Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use('marked', Marked)

new Vue({
  el: '#app',
  render: h => h(App)
}).$mount('#app')
